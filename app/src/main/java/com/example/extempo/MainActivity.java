package com.example.extempo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private final static String[] grupos = { "GRUPO", "01", "02", "03" };
    private Spinner spnGrupos;
    private Button btnSalir;
    private Button btnRegistrar;
    private String seleccion; //elemento seleccionado combobox

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSalir = (Button) findViewById(R.id.btnSalir);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);

        // para poder saber que elemento se selecciono del combobox así como rellenarlo
        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, grupos);
        spnGrupos = (Spinner) findViewById(R.id.cmbGrupos);
        spnGrupos.setAdapter(adapter);
        spnGrupos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                seleccion = spnGrupos.getSelectedItem().toString(); //Recuperamos el texto del elemento seleccionado
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (seleccion.equals("GRUPO")) {
                    Toast.makeText(MainActivity.this, "Por favor seleccione un grupo", Toast.LENGTH_SHORT).show();
                }
                // si selecciona 01 02 03
                else {
                    Intent intent = new Intent(MainActivity.this, RegistrarCalif.class);

                    //Enviar un dato String
                    intent.putExtra("seleccion", seleccion); //se envia el grupo seleccionado

                    startActivity(intent);
                }

            }
        });

    }

}