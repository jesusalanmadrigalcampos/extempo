package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class Calificacion {
    private Context context;
    private DBHelper DBHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[] {
        DefinirTabla.Alumno._ID,
        DefinirTabla.Alumno.MATRICULA,
        DefinirTabla.Alumno.CALIFICACION,
        DefinirTabla.Alumno.MATERIA,
        DefinirTabla.Alumno.GRUPO
    };

    public Calificacion(Context context) {
        this.context = context;
        this.DBHelper = new DBHelper(this.context);
    }

    public void openDatabase() {
        db = DBHelper.getWritableDatabase();
    }

    public long insertarCalif(Alumno c) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Alumno.MATRICULA, c.getMatricula());
        values.put(DefinirTabla.Alumno.CALIFICACION, c.getCalificacion());
        values.put(DefinirTabla.Alumno.MATERIA, c.getMateria());
        values.put(DefinirTabla.Alumno.GRUPO, c.getGrupo());

        return db.insert(DefinirTabla.Alumno.TABLE_NAME, null, values);
    }

    public Alumno leerAlumno(Cursor cursor) {
        Alumno a = new Alumno();

        a.setId(cursor.getInt(0));
        a.setMatricula(cursor.getString(1));
        a.setCalificacion(cursor.getString(2));
        a.setMateria(cursor.getString(3));
        a.setGrupo(cursor.getString(4));

        return a;
    }

    public ArrayList<Alumno> allCalifAlumnos(String grupo) {
        String[] args = new String[] { grupo };

        ArrayList<Alumno> alumnos = new ArrayList<Alumno>();
        Cursor cursor = db.query(DefinirTabla.Alumno.TABLE_NAME,
                null, "grupo=?", args,
                null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Alumno c = leerAlumno(cursor);
            alumnos.add(c);
            cursor.moveToNext();
        }

        cursor.close();
        return alumnos;
    }

    public void cerrar() {
        DBHelper.close();
    }

}
